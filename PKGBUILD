# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Michael Kanis <mkanis_at_gmx_dot_de>

pkgbase=mutter
pkgname=(mutter-mobile)
pkgver=45.0+r431+g742ac7a4f
pkgrel=1.0
pkgdesc="A window manager for GNOME"
url="https://gitlab.gnome.org/GNOME/mutter"
arch=(x86_64 aarch64)
license=(GPL)
depends=(
  colord
  dconf
  gnome-desktop-4
  gnome-settings-daemon
  graphene
  gsettings-desktop-schemas
  iio-sensor-proxy
  lcms2
  libcanberra
  libdisplay-info
  libei
  libgudev
  libinput
  libsm
  libsysprof-capture
  libxkbcommon-x11
  libxkbfile
  pipewire
  startup-notification
  xorg-xwayland
)
makedepends=(
  egl-wayland
  gi-docgen
  git
  gobject-introspection
  gtk3
  meson
  sysprof
  wayland-protocols
  xorg-server
  xorg-server-xvfb
)
checkdepends=(
  gnome-session
  python-dbusmock
  wireplumber
  zenity
)
options=(!debug)
_commit=742ac7a4fe1cacb1ff1ee21f132947c19b9498b7  # mobile-shell-devel
source=("git+https://gitlab.gnome.org/verdre/mobile-mutter.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd mobile-mutter
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd mobile-mutter
  git config --global user.email "build@manjaro.org"
  git config --global user.name "Manjaro Build Server"
  git tag -a 45.0 4f6c91847088d7d6476b88575b3a6601b819b443 -m "Bump version to 45.0"
}

build() {
  CFLAGS="${CFLAGS/-O2/-O3} -fno-semantic-interposition"
  LDFLAGS+=" -Wl,-Bsymbolic-functions"

  arch-meson mobile-mutter build \
    -D egl_device=true \
    -D wayland_eglstream=true \
    -D docs=false \
    -D libdisplay_info=true \
    -D installed_tests=false
  meson compile -C build
}

_check() (
  mkdir -p -m 700 "${XDG_RUNTIME_DIR:=$PWD/runtime-dir}"
  glib-compile-schemas "${GSETTINGS_SCHEMA_DIR:=$PWD/build/data}"
  export XDG_RUNTIME_DIR GSETTINGS_SCHEMA_DIR

  pipewire &
  _p1=$!

  wireplumber &
  _p2=$!

  trap "kill $_p1 $_p2; wait" EXIT

  meson test -C build --print-errorlogs -t 3
)

#check() {
#  dbus-run-session xvfb-run -s '-nolisten local +iglx -noreset' \
#    bash -c "$(declare -f _check); _check"
#}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_mutter-mobile() {
  provides=(libmutter-13.so mutter=$pkgver)
  conflicts=(mutter)
  #groups=(gnome)

  meson install -C build --destdir "$pkgdir"

  _pick docs "$pkgdir"/usr/share/mutter-*/doc
}

# vim:set sw=2 et:
